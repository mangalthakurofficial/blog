import React, { useState } from "react";
import TextField from '@mui/material/TextField';

import Header from "../../components/Header";
import Footer from "../../components/Footer";
import prequoteBanner from "../.././../static/img/prequote-banner.svg"




const PolicyIndexPage = () => {

    const [stepForm, setStepForm] = useState(1)
    const [show, setShow] = useState(false)
    const [existingPolicy, setExistingPolicy] = useState("")
    const [insuredTypePolicy, setInsuredTypePolicy] = useState("")
    const [coverageType, setCoverageType] = useState("")
    const [coverageTypeDto, setCoverageTypeDto] = useState({})

    const [stepFormDto, setStepFormDto] = useState({
        companyName: {
            value: "",
            error: true
        },
        email: {
            value: "",
            error: true
        },
        mobileNumber: {
            value: "",
            error: true
        },
        placeSerach: {
            value: "",
            error: true
        },
        employeeNo: {
            value: "",
            error: true
        }
    })

    const validateEmail = (email) => {
        return String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
    };

    const handleSelectEmployee = (value) => {
        setStepFormDto((prevState) => ({
            ...prevState,
            employeeNo: {
                error: value !== "" ? false : true,
                value: value
            },
        }));
    }


    const handleChange = (e) => {
        const { name, value } = e.target
        let error = true
        if (name === "mobileNumber") {
            if (value.length === 10) {
                error = false
            }
        }
        else if (name === "email") {
            console.log(validateEmail(value))
            if (validateEmail(value)) {
                error = false
            }
        }

        else if (value !== "") {
            error = false
        }
        setStepFormDto((prevState) => ({
            ...prevState,
            [name]: {
                error: error,
                value: value
            },
        }));
    }

    const onKeyPressed = (e) => {
        const key = e.keyCode ? e.keyCode : e.which;

        if (
            !(
                [8, 9, 13, 27, 46].indexOf(key) !== -1 ||
                (key === 65 && (e.ctrlKey || e.metaKey)) ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                (key >= 96 && key <= 105)
            )
        )
            e.preventDefault();
    };


    const handleSubmit = () => {
        setShow(true)
        if ((stepFormDto.companyName.error || stepFormDto.mobileNumber.error || stepFormDto.email.error || stepFormDto.placeSerach.error || stepFormDto.employeeNo.error) && stepForm === 1) {
            return false
        }

        if ((existingPolicy === "" || (Number.parseInt(existingPolicy) === 1 && insuredTypePolicy === "")) && stepForm === 2) {
            return false
        }
        setShow(false)

        setStepForm(stepForm < 3 ? stepForm + 1 : 3)
    }

    const handleSelectOption = (name, value) => {
        console.log("name, value", name, value)
        if (name === "existingPolicy") {
            setExistingPolicy(value)
        } else if (name === "insuredTypePolicy") {
            setInsuredTypePolicy(value)
        } else if (name === "coverageType") {
            setCoverageType(value)
        }

    }

    const handleChangeCoverType = (e) => {
        const { name, value } = e.target.value
        setCoverageTypeDto((prevState) => ({
            ...prevState,
            [name]: value

        }));
    }

    const backStep = () => {
        setShow(true)
        setStepForm(stepForm > 1 ? stepForm - 1 : 1)
    }
    return (
        <>
            <Header />

            <div className="container policy-container">
                <div className="row">
                    <div className="col-md-7">
                        <div className="back"><svg onClick={() => backStep()} className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"></path></svg></div>
                        <h1>Tell us about your Company <span>step {stepForm}/3</span></h1>

                        <form >
                            {stepForm === 1 && <div className="step-one">

                                <div className="mb-3 mt-3">
                                    <TextField
                                        error={show && stepFormDto.companyName.error}
                                        id="outlined-error-helper-text"
                                        label="Company Name"
                                        name="companyName"
                                        value={stepFormDto.companyName.value}
                                        onChange={(e) => handleChange(e)}
                                        helperText={show && stepFormDto.companyName.error ? "Please enter the full company name." : ""}
                                        variant="outlined"
                                        className="form-control"
                                    />
                                </div>
                                <div className="mb-3 mt-3">
                                    <TextField
                                        error={show && stepFormDto.mobileNumber.error}
                                        id="outlined-error-helper-text"
                                        label="Mobile No"
                                        name="mobileNumber"
                                        value={stepFormDto.mobileNumber.value}
                                        onChange={(e) => handleChange(e)}
                                        helperText={show && stepFormDto.mobileNumber.error ? "Please enter 10 digit mobile no." : ""}
                                        variant="outlined"
                                        onKeyDown={onKeyPressed}
                                        className="form-control"
                                    />

                                </div>
                                <div className="mb-3 mt-3">
                                    <TextField
                                        error={show && stepFormDto.email.error}
                                        id="outlined-error-helper-text"
                                        label="Email"
                                        name="email"
                                        value={stepFormDto.email.value}
                                        onChange={(e) => handleChange(e)}
                                        helperText={show && stepFormDto.email.error ? stepFormDto.email.value !== "" ? "Please enter the valid email" : "Email id is required." : ""}
                                        variant="outlined"
                                        className="form-control"

                                    />

                                </div>
                                <div className="mb-3">
                                    <TextField
                                        error={show && stepFormDto.placeSerach.error}
                                        id="outlined-error-helper-text"
                                        label="Location"
                                        name="placeSerach"
                                        value={stepFormDto.placeSerach.value}
                                        onChange={(e) => handleChange(e)}
                                        helperText={show && stepFormDto.placeSerach.error ? "Tell us where you are located." : ""}
                                        variant="outlined"
                                        className="form-control"
                                    />

                                </div>

                                <div className="landing_form_input gmc-lives-field"><span>Select No. of Employees in your organisation</span>
                                    <button type="button" className={stepFormDto.employeeNo.value === 1 ? "lives-button-gmc active-live-button-gmc" : "lives-button-gmc"} onClick={() => handleSelectEmployee(1)}>7-50</button>
                                    <button type="button" className={stepFormDto.employeeNo.value === 2 ? "lives-button-gmc active-live-button-gmc" : "lives-button-gmc"} onClick={() => handleSelectEmployee(2)}>51-200</button>
                                    <button type="button" className={stepFormDto.employeeNo.value === 3 ? "lives-button-gmc active-live-button-gmc" : "lives-button-gmc"} onClick={() => handleSelectEmployee(3)}>200+</button>
                                    {show && stepFormDto.employeeNo.error && <p className="error-message">Please select one option</p>}
                                </div>
                            </div>}

                            {stepForm === 2 && <div className="step-two">
                                <ul className="fresh-or-renewal">
                                    <li className=""
                                        aria-hidden="true"
                                        onKeyPress={() => handleSelectOption("existingPolicy", 1)}
                                        onClick={() => handleSelectOption("existingPolicy", 1)}>
                                        <img alt="" src="https://static.pbcdn.in/sme-cdn/images/fresh-policy-icon.svg" />
                                        <p>
                                            <span>Yes</span> Buying for the first time</p>
                                        <input type="radio" className="form-check-input" id="existingPolicy1" name="existingPolicy" value={1} checked={Number.parseInt(existingPolicy) === 1} onChange={(e) => handleSelectOption("existingPolicy", e.target.value)} /></li>
                                    <li
                                        aria-hidden="true"
                                        className="active" onKeyPress={() => handleSelectOption("existingPolicy", 2)}
                                        onClick={() => handleSelectOption("existingPolicy", 2)}><img alt="" src="https://static.pbcdn.in/sme-cdn/images/renewal-policy-icon.svg" />
                                        <p>
                                            <span>No</span> Existing policy is expiring</p>
                                        <input type="radio" className="form-check-input" id="existingPolicy2" name="existingPolicy" value={2} checked={Number.parseInt(existingPolicy) === 2} onChange={(e) => handleSelectOption("existingPolicy", e.target.value)} />
                                    </li>
                                </ul>
                                {Number.parseInt(existingPolicy) === 1 && <div className="flat-or-grader"><h2>Select Sum Insured Type</h2>
                                    <ul className="fresh-or-renewal">
                                        <li aria-hidden="true" onKeyPress={() => handleSelectOption("insuredTypePolicy", 1)}
                                            onClick={() => handleSelectOption("insuredTypePolicy", 1)}><img alt="" src="https://static.pbcdn.in/sme-cdn/images/icons/flat-icon.svg" />
                                            <p><span>Flat</span> Common sum-insured for all members in the policy</p>
                                            <input type="radio" className="form-check-input" id="insuredTypePolicy1" name="insuredTypePolicy" value={1} checked={Number.parseInt(insuredTypePolicy) === 1} onChange={(e) => handleSelectOption("insuredTypePolicy", e.target.value)} />
                                        </li>
                                        <li aria-hidden="true"
                                            className="active" onKeyPress={() => handleSelectOption("insuredTypePolicy", 2)}
                                            onClick={() => handleSelectOption("insuredTypePolicy", 2)}><img alt="" src="https://static.pbcdn.in/sme-cdn/images/icons/graded-icon.svg" />
                                            <p><span>Graded</span> Different sum-insured for different members in the policy</p>
                                            <input type="radio" className="form-check-input" id="insuredTypePolicy1" name="insuredTypePolicy" value={2} checked={Number.parseInt(insuredTypePolicy) === 2} onChange={(e) => handleSelectOption("insuredTypePolicy", e.target.value)} />
                                        </li>
                                    </ul>
                                </div>}
                                {show && (existingPolicy === "" || (Number.parseInt(existingPolicy) === 1 && insuredTypePolicy === "")) && <p className="form-control error-message">Please select one option</p>}
                                {Number.parseInt(insuredTypePolicy) === 1 && <>
                                    <h2>How much sum insured do you want per employee?</h2>
                                    <select className="form-select mt-3">
                                        <option>1 lac</option>
                                        <option>2 lac</option>
                                        <option>3 lac</option>
                                        <option>4 lac</option>
                                        <option>5 lac</option>
                                    </select>
                                    <div className="gmc-try-calculator_notSure mobilehidediv">
                                        <p>Not sure about the sum insured?</p>
                                        <button>Try calculator</button>
                                    </div>
                                </>}
                            </div>}
                            {stepForm === 3 &&
                                <div className="step-three">
                                    <ul className="fresh-or-renewal-pq3 pl-0">
                                        <li
                                            aria-hidden="true"
                                            onKeyPress={() => handleSelectOption("coverageType", 1)}
                                            onClick={() => handleSelectOption("coverageType", 1)}
                                        >
                                            <p><span>Employee only</span>
                                                <input type="radio" className="form-check-input" id="coverageType1" name="coverageType" value={1} checked={Number.parseInt(coverageType) === 1} onChange={(e) => handleSelectOption("coverageType", e.target.value)} />
                                            </p>
                                            <div className="coverage-image-style coverage-image-style-employee"><img src="https://static.pbcdn.in/sme-cdn/images/employeeOnly.svg" className="coverage-employee" alt="img" /></div>
                                        </li>
                                        <li
                                            aria-hidden="true"
                                            onKeyPress={() => handleSelectOption("coverageType", 2)}
                                            onClick={() => handleSelectOption("coverageType", 2)} className="active active-coverage-border">
                                            <p><span>Employee, Spouse &amp; 2 Kids</span>
                                                <input type="radio" className="form-check-input" id="coverageType2" name="coverageType" value={2} checked={Number.parseInt(coverageType) === 2} onChange={(e) => handleSelectOption("coverageType", e.target.value)} />
                                            </p>
                                            <div className="coverage-image-style coverage-image-style-esk"><img src="https://static.pbcdn.in/sme-cdn/images/employeeSpouse.svg" className="coverage-employee" alt="img" /></div></li>
                                        <li
                                            aria-hidden="true"
                                            onKeyPress={() => handleSelectOption("coverageType", 3)}
                                            onClick={() => handleSelectOption("coverageType", 3)}>
                                            <p>
                                                <span>Employee, Spouse, 2 Kids &amp; Parents</span>
                                                <input type="radio" className="form-check-input" id="coverageType3" name="coverageType" value={3} checked={Number.parseInt(coverageType) === 3} onChange={(e) => handleSelectOption("coverageType", e.target.value)} />
                                            </p><div className="coverage-image-style coverage-image-style-eskp"><img src="https://static.pbcdn.in/sme-cdn/images/employeeSpouseKids.svg" className="coverage-employee" alt="img" /></div></li>
                                    </ul>

                                    {Number.parseInt(coverageType) === 1 && <>
                                        <div className="mb-3">
                                            <TextField
                                                // error={stepFormDto.companyName.error}
                                                label="Total number Of Employees"
                                                name="numberOfEmployee"
                                                value={coverageTypeDto.numberOfEmployee}
                                                onChange={(e) => handleChangeCoverType(e)}
                                                // helperText={show && stepFormDto.companyName.error ? "Please enter the full company name." : ""}
                                                variant="outlined"
                                                className="form-control"
                                            />

                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <TextField
                                                    label="19-35 years"
                                                    name="numberOfEmployee19"
                                                    value={coverageTypeDto.numberOfEmployee19}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="36-45 years"
                                                    name="numberOfEmployee36"
                                                    value={coverageTypeDto.numberOfEmployee36}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>
                                        <div className="row mt-3">
                                            <div className="col">
                                                <TextField
                                                    label="46-55 years"
                                                    name="numberOfEmployee46"
                                                    value={coverageTypeDto.numberOfEmployee46}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="56-65 years"
                                                    name="numberOfEmployee56"
                                                    value={coverageTypeDto.numberOfEmployee56}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>
                                    </>
                                    }

                                    {coverageType !== "" && Number.parseInt(coverageType) !== 1 && <>


                                        <div className="mb-3 mt-4">

                                            <TextField
                                                // error={stepFormDto.companyName.error}
                                                label="Total number Of Lives"
                                                name="numberOfLive"
                                                value={coverageTypeDto.numberOfLive}
                                                onChange={(e) => handleChangeCoverType(e)}
                                                // helperText={show && stepFormDto.companyName.error ? "Please enter the full company name." : ""}
                                                variant="outlined"
                                                className="form-control"
                                            />


                                        </div>
                                        <h4 className="mb-4">Edit Employee Count</h4>
                                        <div className="row">
                                            <div className="col">
                                                <TextField
                                                    label="19-35 years"
                                                    name="numberOfLiveEmployee19"
                                                    value={coverageTypeDto.numberOfLiveEmployee19}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="36-45 years"
                                                    name="numberOfLiveEmployee36"
                                                    value={coverageTypeDto.numberOfLiveEmployee36}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                        <div className="row mt-3">
                                            <div className="col">
                                                <TextField
                                                    label="46-55 years"
                                                    name="numberOfLiveEmployee46"
                                                    value={coverageTypeDto.numberOfLiveEmployee46}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="56-65 years"
                                                    name="numberOfLiveEmployee56"
                                                    value={coverageTypeDto.numberOfLiveEmployee56}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                        <h4 className="mb-4">Edit Spouse Count</h4>
                                        <div className="row">
                                            <div className="col">
                                                <TextField
                                                    label="19-35 years"
                                                    name="numberOfLiveSpouse19"
                                                    value={coverageTypeDto.numberOfLiveSpouse19}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="36-45 years"
                                                    name="numberOfLiveSpouse36"
                                                    value={coverageTypeDto.numberOfLiveSpouse36}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                        <div className="row mt-3">
                                            <div className="col">
                                                <TextField
                                                    label="46-55 years"
                                                    name="numberOfLiveSpouse46"
                                                    value={coverageTypeDto.numberOfLiveSpouse46}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="col">
                                                <TextField
                                                    label="56-65 years"
                                                    name="numberOfLiveSpouse56"
                                                    value={coverageTypeDto.numberOfLiveSpouse56}
                                                    onChange={(e) => handleChangeCoverType(e)}
                                                    variant="outlined"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                        {Number.parseInt(coverageType) === 3 && <>
                                            <h4 className="mb-4">Edit Parents Count</h4>
                                            <div className="row">
                                                <div className="col">
                                                    <TextField
                                                        label="36-45 years"
                                                        name="numberOfLiveParent36"
                                                        value={coverageTypeDto.numberOfLiveParent36}
                                                        onChange={(e) => handleChangeCoverType(e)}
                                                        variant="outlined"
                                                        className="form-control"
                                                    />
                                                </div>
                                                <div className="col">
                                                    <TextField
                                                        label="46-55 years"
                                                        name="numberOfLiveParent46"
                                                        value={coverageTypeDto.numberOfLiveParent46}
                                                        onChange={(e) => handleChangeCoverType(e)}
                                                        variant="outlined"
                                                        className="form-control"
                                                    />
                                                </div>
                                            </div>

                                            <div className="row mt-3">

                                                <div className="col">
                                                    <TextField
                                                        label="56-65 years"
                                                        name="numberOfLiveParent56"
                                                        value={coverageTypeDto.numberOfLiveParent56}
                                                        onChange={(e) => handleChangeCoverType(e)}
                                                        variant="outlined"
                                                        className="form-control"
                                                    />
                                                </div>
                                                <div className="col">
                                                    <TextField
                                                        label="65+ years"
                                                        name="numberOfLiveParent66"
                                                        value={coverageTypeDto.numberOfLiveParent66}
                                                        onChange={(e) => handleChangeCoverType(e)}
                                                        variant="outlined"
                                                        className="form-control"
                                                    />
                                                </div>
                                            </div>
                                        </>
                                        }

                                        <h4 className="mb-4">Edit Kids Count</h4>


                                        <div className="mb-3 mt-4">
                                            <TextField
                                                label="Total Kids"
                                                name="totalKid"
                                                value={coverageTypeDto.totalKid}
                                                onChange={(e) => handleChangeCoverType(e)}
                                                variant="outlined"
                                                className="form-control"
                                            />

                                        </div>
                                    </>}

                                </div>}
                            <button type="button" className="prequote-btn" onClick={() => handleSubmit()}>Continue</button>

                            {stepForm === 1 && <div className="whatsapp-updates">
                                <p><img alt="" src="https://static.pbcdn.in/sme-cdn/images/whatsapp-new.svg" /> Get WhatsApp updates</p>
                                <div className="form-check form-switch">
                                    <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" />
                                </div>
                            </div>
                            }

                        </form>
                    </div>
                    {stepForm !== 3 && <div className="col-md-5 prequote-banner text-center">
                        <img src={prequoteBanner} alt="img" />
                        <h2>Affordable Premiums</h2>
                        <p>Compared to offline insurance, we offer higher coverage at a lower premium</p>
                    </div>}
                </div>
            </div>

            <Footer />
        </>
    );
}

export default PolicyIndexPage