import * as React from "react";
import { Link } from "gatsby";

import logo from "../../static/img/logo.svg"
import tollfree from "../../static/img/tollfree.svg";

const Footer = class extends React.Component {
    render() {
        return (
            <header className="navbar navbar-expand-sm navbar-light">
                <div className="container">
                    <Link to="/policy-bazar" className="navbar-brand">
                        <img className="img-fluid" src={logo} alt="logo"/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="collapsibleNavbar">


                        <ul className="navbar-nav numbers-top">
                            <li className="nav-item">
                                <img alt="" src={tollfree} /> Sales &amp; Services: <span><a rel="noopener" href="tel:1800-572-3918">1800-572-3918</a></span>
                            </li>
                        </ul>

                    </div>
                </div>
            </header>
        );
    }
};

export default Footer;
