import * as React from "react";

const Footer = class extends React.Component {
  render() {
    return (
      <footer>
        <div className="container-fluid legal-footer py-3">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="legal-footer-in"><p className="disclaimer-footer-text">Disclaimer</p>
                  <div>Policybazaar Insurance Brokers Private Limited (formerly known as Policybazaar Insurance Web Aggregator Private Limited) | CIN: U74999HR2014PTC053454 | Registered Office - Plot No.119, Sector - 44, Gurgaon, Haryana – 122001 <br />
                    <div> <a target="_blank" rel="noreferrer" href="https://www.policybazaar.com/contact-us/">Contact Us</a>  |  <a target="_blank" rel="noreferrer" href="https://www.policybazaar.com/legal-and-admin-policies/">Legal and Admin Policies</a> </div></div><p>Policybazaar is now registered as a Direct Broker | Registration No. 742, Registration Code No. IRDA/ DB 797/ 19, Valid till 09/06/2024, License category- Direct Broker (Life &amp; General) | Visitors are hereby informed that their information submitted on the website may be shared with insurers. Product information is authentic and solely based on the information received from the insurers.</p>
                  <p>© Copyright 2008-2021 policybazaar.com. All Rights Reserved.</p></div>


              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
};

export default Footer;
